package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = (ListView) findViewById(R.id.listviewJsonData); //根據你設的ID
        String url2 = "https://api.data.gov.hk/v1/carpark-info-vacancy?lang=en_US";
        getData(url2); //取得網址字串

    }

    private String getData(String urlString) {
        String result = "";
        //使用JsonObjectRequest類別要求JSON資料。
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(urlString, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            //Velloy採非同步作業，Response.Listener  監聽回應
                            public void onResponse(JSONObject response) {
                                Log.d("回傳結果", "結果=" + response.toString());
                                try {
                                    parseJSON(response);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Response.ErrorListener 監聽錯誤
                        Log.e("回傳結果", "錯誤訊息：" + error.toString());
                    }
                });
        Volley.newRequestQueue(this).add(jsonObjectRequest);
        return result;
    }

    private void parseJSON(JSONObject jsonObject) throws JSONException {
        ArrayList<String> list = new ArrayList();
//        JSONArray data = jsonObject.getJSONObject("result").getJSONArray("results");
//        JSONArray data = jsonObject.getJSONObject("results").getJSONArray("park_Id");
//        for (int i = 0; i < data.length(); i++) {
//            JSONObject o = data.getJSONObject(i);
//            String str = "park_Id:" + o.getString("park_Id") + "\n"
//                    + "name:" + o.getString("name") + "\n";
//            list.add(str);
//        }

        JSONArray rs = jsonObject.getJSONArray("results");
        for (int i = 0; i < rs.length(); i++) {
            JSONObject o = rs.getJSONObject(i);
            String str = "park_Id:" + o.getString("park_Id") + "\n"
                    + "name:" + o.getString("name") + "\n";
            list.add(str);
        }



        lv.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list));
    }
}